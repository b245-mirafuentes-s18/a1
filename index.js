/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function
*/
		function sumNumber(){
			console.log("Sum of", 5, "and", 15);
			console.log(5 + 15);
		}
		function invokeSum(sum){
			sum()
		}
		invokeSum(sumNumber);

		function differenceNumber(){
			console.log("Difference of", 20, "and", 5);
			console.log(5 + 15);
		}
		function invokeDifference(difference){
			difference()
		}
		invokeDifference(differenceNumber);

/*	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.
*/

		function MultiplicationNumber(num1, num2){
			let product = num1 * num2
			console.log("Product of", num1, "and", num2);
			console.log(product);
		}
		function resultProduct(num1, num2){
			return product
		}
		MultiplicationNumber(50, 10);

		function divisionNumber(num3, num4){
			let quotient = num3 / num4;
			console.log("Quotient of", num3, "and", num4);
			console.log(quotient);
		}
		function resultQuotient(num3, num4){
			return quotient
		}
		Number(50, 10);
;

/*	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.
*/

		function area(num5){
			let cArea = 3.14159265359 * (num5 ** 2)
			console.log("The result of getting the area of a circle with", num5, "radius");
			console.log(cArea);
		}
		function circleArea(num5){
			return cArea
		}
		area(15);

/*	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.*/
		
		function average(num6, num7, num8, num9){
			let tAverage = (num6 + num7 + num8 + num9) / 4;
			console.log("The average of", num6, num7, num8, num9);
			console.log(tAverage);
		}
		function averageVar(num6, num7, num8, num9){
			return tAverage;
		}
		average(20, 40, 60, 80);

/*	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/
		function passing(num10, num11){
			let percentage = (num10 / num11) * 100;
			console.log("Is", num10, "/", num11, "a passing score?");
			let passingPercentage = percentage >= 75;
			console.log(passingPercentage);
		}

		function pass(num10, num11){
			return percentage;
		}
		passing(37, 50);